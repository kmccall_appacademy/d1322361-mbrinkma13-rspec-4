class Dictionary
  # TODO: your code goes here!

  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(Hash)
      @entries.merge!(entry)
    elsif entry.is_a?(String)
      @entries[entry] = nil
    end
  end

  def keywords
    entries.keys.sort
  end

  def include?(keyword)
    keywords.include?(keyword)
  end

  def find(word)
    if entries[word]
      entries.select { |k, _v| k.include?(word) }
    else
      entries.select { |k, _v| k.match(word) }
    end
  end

  def printable
    printable = keywords.map do |k|
      entry = @entries[k]
      "[" + "#{k}" + "] " + %Q{"#{entry}"}
    end

    printable.join("\n")
  end

end
