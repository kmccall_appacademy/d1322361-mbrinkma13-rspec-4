class Temperature
  # TODO: your code goes here!
  def initialize(options)
    if options[:f]
      @fahrenheit = options[:f]
    elsif options[:c]
      @celsius = options[:c]
    end
  end

  attr_accessor :fahrenheit, :celsius, :in_celcius

  def in_celsius
    if @celsius
      @celsius
    elsif @fahrenheit
      (@fahrenheit - 32) * (5.to_f/9.to_f)
    end
  end

  def in_fahrenheit
    if @fahrenheit
      @fahrenheit
    elsif @celsius
      @celsius * (9.to_f/5.to_f) + 32
    end
  end

  def self.from_celsius(celsius)
    self.new(c: celsius)
  end

  def self.from_fahrenheit(fahrenheit)
    self.new(f: fahrenheit)
  end
end

class Celsius < Temperature
  def initialize(temperature)
    @celsius = temperature
  end
  attr_accessor :celsius
end

class Fahrenheit < Temperature
  def initialize(temperature)
    @fahrenheit = temperature
  end
  attr_accessor :fahrenheit
end
