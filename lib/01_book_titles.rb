class Book
  # TODO: your code goes here!
  attr_accessor :title

  def initialize(title=nil)
    @title = title
  end

  def title
    capitalized = []
    no_cap = ['and', 'of', 'the', 'in', 'a', 'an']
    @title.split.each_with_index do |w, i|
      if i == 0
        capitalized.push(w.capitalize)
      elsif no_cap.include?(w)
        capitalized.push(w)
      else
        capitalized.push(w.capitalize)
      end
    end
    capitalized.join(' ')
  end

end
