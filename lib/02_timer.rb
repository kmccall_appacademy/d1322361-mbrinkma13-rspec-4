# require 'byebug'

class Timer
  def initialize(seconds = 0)
    @seconds = seconds
  end

  attr_accessor :seconds

  def time_string
    # debugger
    hours = 0
    minutes = 0
    time_string = ''

    while @seconds >= 3600
      hours += 1
      @seconds -= 3600
    end
    while @seconds >= 60
      minutes += 1
      @seconds -= 60
    end

    if hours < 10
      time_string << "0#{hours}:"
    else
      time_string << "#{hours}:"
    end
    if minutes < 10
      time_string << "0#{minutes}:"
    else
      time_string << "#{mintues}:"
    end
    if seconds < 10
      time_string << "0#{seconds}"
    else
      time_string << "#{seconds}"
    end

    time_string
  end

end
