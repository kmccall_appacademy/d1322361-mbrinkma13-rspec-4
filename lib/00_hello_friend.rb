class Friend
  # TODO: your code goes here!

  attr_accessor :greeting

  def greeting(name=nil)
    if name
      "Hello, #{name}!"
    else
      "Hello!"
    end
  end

end
